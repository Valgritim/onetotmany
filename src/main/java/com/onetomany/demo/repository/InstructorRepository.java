package com.onetomany.demo.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.onetomany.demo.model.Instructor;

@Repository
public interface InstructorRepository extends JpaRepository<Instructor, Long>{	
	
}
