package com.onetomany.demo;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.onetomany.demo.model.Instructor;
import com.onetomany.demo.repository.InstructorRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
class InstructorControllerTest {
    @Autowired
    private InstructorRepository instructorRepository;
    @Autowired
    private TestEntityManager entityManager;
	@Test
	void testGetInstructors() {
		Instructor inst1 = new Instructor("admin","admin","admin@admin");
		Instructor inst2 = new Instructor("user","user","user@user");
		entityManager.persist(inst1);
		entityManager.persist(inst2);
		List<Instructor> instructorFromDb = instructorRepository.findAll();
		assertThat(instructorFromDb.size()).isEqualTo(2);
	}
	@Test
	void testGetInstructorById() {
		Instructor inst = new Instructor("admin","admin","admin@admin.fr");
		Instructor instructorSavedInDb = entityManager.persist(inst);
		Instructor instructorFromDb = instructorRepository.getOne(instructorSavedInDb.getId());
		assertEquals(instructorSavedInDb,instructorFromDb);
		assertThat(instructorSavedInDb.equals(instructorFromDb));
	}
	@Test
	void testCreateUser() {
		Instructor inst = new Instructor("admin","admin","admin@admin.fr");
		Instructor instructorSavedInDb = entityManager.persist(inst);
		Instructor instructorFromDb = instructorRepository.getOne(instructorSavedInDb.getId());
		assertEquals(instructorSavedInDb,instructorFromDb);
		assertThat(instructorSavedInDb.equals(instructorFromDb));
	}
	@Test
	void testUpdateUser() {
		Instructor inst = new Instructor("admin","admin","admin@admin.fr");
		entityManager.persist(inst);
		Instructor getFromDb = instructorRepository.getOne(inst.getId());
		getFromDb.setFirstName("admino");
		entityManager.persist(getFromDb);
		Instructor getFromDbNew = instructorRepository.getOne(inst.getId());
		assertThat(getFromDbNew.getFirstName()).isEqualTo("admino");
	}
	@Test
	void testDeleteUser() {
		Instructor inst1 = new Instructor("admin","admin","admin@admin.fr");
		Instructor inst2 = new Instructor("user","user","user@user.fr");
		Instructor inst = entityManager.persist(inst1);
		entityManager.persist(inst2);
		entityManager.remove(inst);
		List<Instructor> AllInstructorsFromDb = instructorRepository.findAll();
		assertThat(AllInstructorsFromDb.size()).isEqualTo(1);
	}
}