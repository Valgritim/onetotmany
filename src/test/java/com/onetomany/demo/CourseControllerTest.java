package com.onetomany.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.onetomany.demo.model.Course;
import com.onetomany.demo.model.Instructor;
import com.onetomany.demo.repository.CourseRepository;
import com.onetomany.demo.repository.InstructorRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
class CourseControllerTest {
    @Autowired
    private InstructorRepository instructorRepository;
    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private TestEntityManager entityManager;

	@Test
	void testGetCoursesByInstructor() {
		Instructor instructor = new Instructor("titi","temar","email@email.com");				
		Course course1 = new Course("Java",instructor );	
		Course course2 = new Course("Jee",instructor );
		entityManager.persist(course1);		
		entityManager.persist(course2);	
		List<Course> courseFromDb = courseRepository.findByInstructorId(instructor.getId());
		assertThat(courseFromDb.size()).isEqualTo(2);
		
	}

	@Test
	void testSave() {
		Instructor instructor = new Instructor("titi","temar","email@email.com");
		Course course1 = new Course("Java",instructor );
		Course course2 = new Course("PHP",instructor );
		entityManager.persist(course2);
		entityManager.persist(course1);
		
		List<Course> coursesFromDb = courseRepository.findAll();
		assertThat(coursesFromDb.size()).isEqualTo(2);
	}

	@Test
	void testUpdateCourse() {
		Instructor instructor = new Instructor("titi","temar","email@email.com");
		Course course1 = new Course("Java",instructor );
		entityManager.persist(course1);
		Course getCourseInDb = courseRepository.getOne(course1.getId());
		getCourseInDb.setTitle("JEE");
		entityManager.persist(getCourseInDb);		
		assertThat(getCourseInDb.getTitle()).isEqualTo("JEE");
		
	}

	@Test
	void testDeleteCourse() {
		Instructor instructor = new Instructor("titi","temar","email@email.com");
		Course course1 = new Course("Java",instructor );
		Course course2 = new Course("PHP",instructor );
		Course persist =entityManager.persist(course1);
		entityManager.persist(course2);
		entityManager.remove(course2);
		
		List<Course> coursesFromDb = courseRepository.findAll();
		assertThat(coursesFromDb.size()).isEqualTo(1);
		
	}

}
